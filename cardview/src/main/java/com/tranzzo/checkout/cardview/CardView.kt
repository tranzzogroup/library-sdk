package com.tranzzo.checkout.cardview

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.util.AttributeSet
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import cards.pay.paycardsrecognizer.sdk.Card
import cards.pay.paycardsrecognizer.sdk.ScanCardIntent
import com.tranzzo.checkout.cardview.cardnumber.CustomCardNumber
import com.tranzzo.checkout.cardview.cvv.CustomCvvEditText
import com.tranzzo.checkout.cardview.expirationdate.CustomExpirationDateEditText
import com.tranzzo.checkout.cardview.result.PaymentCard
import com.tranzzo.checkout.cardview.result.PaymentCardResult

class CardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var mCardNumber: CustomCardNumber
    var mExpiration: CustomExpirationDateEditText
    var mCvv: CustomCvvEditText

    init {
        inflate(context, R.layout.bt_card_form_fields, this)
        mCardNumber = findViewById(R.id.bt_card_form_card_number)
        mExpiration = findViewById(R.id.bt_card_form_expiration)
        mCvv = findViewById(R.id.bt_card_form_cvv)
        initIconScanCardNumber()
        setOnClickCardScan()
    }

    fun validateSelf(): PaymentCard? {
        if (!mCardNumber.isValid) {
            mCardNumber.setError(context.getString(R.string.error_enter_not_correct_card))
            return null
        }

        if (!mExpiration.isValid) {
            mExpiration.setError(context.getString(R.string.error_enter_date))
            return null
        }

        if (!mCvv.isValid) {
            mCvv.setError(context.getString(R.string.error_enter_cvv))
            return null
        }
        return getCard()
    }

    fun getPaymentCard(): PaymentCardResult<PaymentCard, List<CardViewError>> {
        val errors: MutableList<CardViewError> = ArrayList()

        if (!mCardNumber.isValid) {
            Integer.valueOf(1)
            errors.add(CardViewError.WRONG_CARD_NUMBER)
        }

        if (!mExpiration.isValid) {
            errors.add(CardViewError.WRONG_DATE)
        }

        if (!mCvv.isValid) {
            errors.add(CardViewError.WRONG_CVV)
        }

        return if (errors.isEmpty()) {
            PaymentCardResult.Success(getCard())
        } else {
            PaymentCardResult.Error(errors)
        }
    }

    private fun getCard() = PaymentCard(
        mCardNumber.text.toString(),
        mExpiration.year,
        mExpiration.month,
        mCvv.text.toString()
    )

    open fun initIconScanCardNumber(@DrawableRes resDrawableId: Int = R.drawable.ic_camera_scan) {
        mCardNumber.textInputLayoutParent?.endIconDrawable =
            ContextCompat.getDrawable(context, resDrawableId)
    }

    private fun setOnClickCardScan() {
        mCardNumber.textInputLayoutParent?.setEndIconOnClickListener {
            scanDataCard { _, cardData ->
                cardData?.let {
                    mCardNumber.setText(it.cardNumber)
                    mExpiration.setText(it.expirationDate)
                }
            }
        }
    }

    fun scanDataCard(dataCardCallback: (actionId: Int, Card?) -> Unit) {
        val intent = ScanCardIntent.Builder(context).build()
        val contract =
            getContext(context)?.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == Activity.RESULT_OK) {
                    val card =
                        it.data?.getSerializableExtra(ScanCardIntent.RESULT_PAYCARDS_CARD) as Card
                    dataCardCallback(0, card)
                } else {
                    val actionId = it.data?.getIntExtra(ScanCardIntent.RESULT_CANCEL_REASON, 0) ?: 0
                    dataCardCallback(actionId, null)
                }
            }
        contract?.launch(intent)
    }

    private fun getContext(cont: Context?): FragmentActivity? =
        when (cont) {
            is FragmentActivity -> cont
            is ContextWrapper -> getContext(
                cont.baseContext
            )
            else -> null
        }


    enum class CardViewError {
        WRONG_CARD_NUMBER, WRONG_DATE, WRONG_CVV
    }
}