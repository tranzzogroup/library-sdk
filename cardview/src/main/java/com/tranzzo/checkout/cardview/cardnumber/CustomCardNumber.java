package com.tranzzo.checkout.cardview.cardnumber;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.tranzzo.checkout.cardview.R;

public class CustomCardNumber  extends CustomErrorEditText implements TextWatcher {

    private CustomCardType mCardType;

    public CustomCardNumber(Context context) {
        super(context);
        init();
    }

    public CustomCardNumber(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomCardNumber(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setInputType(InputType.TYPE_CLASS_NUMBER);
        addTextChangedListener(this);
        updateCardType();
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if (focused) {

            if (getText().toString().length() > 0) {
                setSelection(getText().toString().length());
            }
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        Object[] paddingSpans = editable.getSpans(0, editable.length(), CustomSpaceSpan.class);
        for (Object span : paddingSpans) {
            editable.removeSpan(span);
        }

        updateCardType();

        addSpans(editable, mCardType.getSpaceIndices());

        if (mCardType.getMaxCardLength() == getSelectionStart()) {
            validate();

            if (isValid()) {
                focusNextView();
            }
        }
    }

    @Override
    public boolean isValid() {
        return mCardType.validate(getText().toString());
    }

    @Override
    public String getErrorMessage() {
            return getContext().getString(R.string.error_enter_not_correct_card);
    }

    private void updateCardType() {
        CustomCardType type = CustomCardType.forCardNumber(getText().toString());
        if (mCardType != type) {
            mCardType = type;

            InputFilter[] filters = { new InputFilter.LengthFilter(mCardType.getMaxCardLength()) };
            setFilters(filters);
            invalidate();
        }
    }

    private void addSpans(Editable editable, int[] spaceIndices) {
        final int length = editable.length();
        for (int index : spaceIndices) {
            if (index <= length) {
                editable.setSpan(new CustomSpaceSpan(), index - 1, index,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
}
