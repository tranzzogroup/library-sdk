package com.tranzzo.checkout.cardview.cardnumber;

import android.text.TextUtils;

import java.util.regex.Pattern;

public enum CustomCardType {

    VISA("^4\\d*",
            16, 16,
            3, null),
    MASTERCARD("^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[0-1]|2720)\\d*",

            16, 16,
            3,  null),
    DISCOVER("^(6011|65|64[4-9]|622)\\d*",

            16, 16,
            3, null),
    AMEX("^3[47]\\d*",

            15, 15,
            4, null),
    DINERS_CLUB("^(36|38|30[0-5])\\d*",

            14, 14,
            3,  null),
    JCB("^35\\d*",

            16, 16,
            3, null),
    MAESTRO("^(5018|5020|5038|5[6-9]|6020|6304|6703|6759|676[1-3])\\d*",
            12, 19,
            3  ,
            "^6\\d*"),
    UNIONPAY("^62\\d*",
            16, 19,
            3,  null),
    HIPER("^637(095|568|599|609|612)\\d*",
            16, 16,
            3,  null),
    HIPERCARD("^606282\\d*",
            16, 16,
            3,   null),
    UNKNOWN("\\d+",
            12, 19,
            3,  null),
    EMPTY("^$",
            12, 19,
            3, null);

    private static final int[] AMEX_SPACE_INDICES = { 4, 10 };
    private static final int[] DEFAULT_SPACE_INDICES = { 4, 8, 12 };

    private final Pattern mPattern;
    private final Pattern mRelaxedPrefixPattern;
    private final int mMinCardLength;
    private final int mMaxCardLength;
    private final int mSecurityCodeLength;

    CustomCardType(String regex, int minCardLength, int maxCardLength, int securityCodeLength, String relaxedPrefixPattern) {
        mPattern = Pattern.compile(regex);
        mRelaxedPrefixPattern = relaxedPrefixPattern == null ? null : Pattern.compile(relaxedPrefixPattern);
        mMinCardLength = minCardLength;
        mMaxCardLength = maxCardLength;
        mSecurityCodeLength = securityCodeLength;
    }

    public static CustomCardType forCardNumber(String cardNumber) {
        CustomCardType patternMatch = forCardNumberPattern(cardNumber);
        if (patternMatch != EMPTY && patternMatch != UNKNOWN) {
            return patternMatch;
        }

        CustomCardType relaxedPrefixPatternMatch = forCardNumberRelaxedPrefixPattern(cardNumber);
        if (relaxedPrefixPatternMatch != EMPTY && relaxedPrefixPatternMatch != UNKNOWN) {
            return relaxedPrefixPatternMatch;
        }

        if (!cardNumber.isEmpty()) {
            return UNKNOWN;
        }

        return EMPTY;
    }

    private static CustomCardType forCardNumberPattern(String cardNumber) {
        for (CustomCardType cardType : values()) {
            if (cardType.getPattern().matcher(cardNumber).matches()) {
                return cardType;
            }
        }

        return EMPTY;
    }

    private static CustomCardType forCardNumberRelaxedPrefixPattern(String cardNumber) {
        for (CustomCardType cardTypeRelaxed : values()) {
            if (cardTypeRelaxed.getRelaxedPrefixPattern() != null) {
                if (cardTypeRelaxed.getRelaxedPrefixPattern().matcher(cardNumber).matches()) {
                    return cardTypeRelaxed;
                }
            }
        }

        return EMPTY;
    }

    /**
     * @return The regex matching this card type.
     */
    public Pattern getPattern() {
        return mPattern;
    }

    /**
     * @return The relaxed prefix regex matching this card type. To be used in determining card type if no pattern matches.
     */
    public Pattern getRelaxedPrefixPattern() {
        return mRelaxedPrefixPattern;
    }

    /**
     * @return The length of the current card's security code.
     */
    public int getSecurityCodeLength() {
        return mSecurityCodeLength;
    }


    public int getMinCardLength() {
        return mMinCardLength;
    }


    public int getMaxCardLength() {
        return mMaxCardLength;
    }

    /**
     * @return the locations where spaces should be inserted when formatting the card in a user
     * friendly way. Only for display purposes.
     */
    public int[] getSpaceIndices() {
        return this == AMEX ? AMEX_SPACE_INDICES : DEFAULT_SPACE_INDICES;
    }

    /**
     * Performs the Luhn check on the given card number.
     *
     * @param cardNumber a String consisting of numeric digits (only).
     * @return {@code true} if the sequence passes the checksum
     * @throws IllegalArgumentException if {@code cardNumber} contained a non-digit (where {@link
     * Character#isDefined(char)} is {@code false}).
     * @see <a href="http://en.wikipedia.org/wiki/Luhn_algorithm">Luhn Algorithm (Wikipedia)</a>
     */
    public static boolean isLuhnValid(String cardNumber) {
        final String reversed = new StringBuffer(cardNumber).reverse().toString();
        final int len = reversed.length();
        int oddSum = 0;
        int evenSum = 0;
        for (int i = 0; i < len; i++) {
            final char c = reversed.charAt(i);
            if (!Character.isDigit(c)) {
                throw new IllegalArgumentException(String.format("Not a digit: '%s'", c));
            }
            final int digit = Character.digit(c, 10);
            if (i % 2 == 0) {
                oddSum += digit;
            } else {
                evenSum += digit / 5 + (2 * digit) % 10;
            }
        }
        return (oddSum + evenSum) % 10 == 0;
    }

    /**
     * @param cardNumber The card number to validate.
     * @return {@code true} if this card number is locally valid.
     */
    public boolean validate(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return false;
        } else if (!TextUtils.isDigitsOnly(cardNumber)) {
            return false;
        }

        final int numberLength = cardNumber.length();
        if (numberLength < mMinCardLength || numberLength > mMaxCardLength) {
            return false;
        } else if (!mPattern.matcher(cardNumber).matches() && mRelaxedPrefixPattern != null && !mRelaxedPrefixPattern.matcher(cardNumber).matches()) {
            return false;
        }
        return isLuhnValid(cardNumber);
    }
}
