package com.tranzzo.checkout.cardview.cvv;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.tranzzo.checkout.cardview.R;
import com.tranzzo.checkout.cardview.cardnumber.CustomErrorEditText;

public class CustomCvvEditText extends CustomErrorEditText implements TextWatcher {

    private static final int DEFAULT_MAX_LENGTH = 3;

    public CustomCvvEditText(Context context) {
        super(context);
        init();
    }

    public CustomCvvEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomCvvEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setInputType(InputType.TYPE_CLASS_NUMBER);
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MAX_LENGTH)});
        setContentDescription(getContext().getString(R.string.bt_cvv));
        setFieldHint(getContext().getString(R.string.bt_cvv));
        addTextChangedListener(this);
    }


    @Override
    public void afterTextChanged(Editable editable) {
            if (isValid()) {
                focusNextView();
            }
    }

    @Override
    public boolean isValid() {
        return getText().toString().length() == getSecurityCodeLength();
    }

    @Override
    public String getErrorMessage() {
        String securityCodeName =  getContext().getString(R.string.bt_cvv);

        if (TextUtils.isEmpty(getText())) {
            return getContext().getString(R.string.bt_cvv_required, securityCodeName);
        } else {
            return getContext().getString(R.string.bt_cvv_invalid, securityCodeName);
        }
    }

    private int getSecurityCodeLength() {
            return DEFAULT_MAX_LENGTH;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
}

