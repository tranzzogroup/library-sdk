package com.tranzzo.checkout.cardview.expirationdate;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.ReplacementSpan;

public class CustomSlashSpan  extends ReplacementSpan {

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        float padding = paint.measureText(" ", 0, 1) * 2;
        float slash = paint.measureText("/", 0, 1);
        float textSize = paint.measureText(text, start, end);
        return (int) (padding + slash + textSize);
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y,
                     int bottom, Paint paint) {
        canvas.drawText(text.subSequence(start, end) + " / ", x, y, paint);
    }
}

