package com.tranzzo.checkout.cardview.result

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

/**
 * @param cardNum - the number for credit card
 * @param expYear - expiration year for credit card
 * @param expMonth - expiration month for credit card
 * @param cardCVV2 - card verification value 2 for credit card
 *
 * @sample:
 * [cardNum] - "1111222233334444"
 * [expYear] - "25"
 * [expMonth] - "12"
 * [cardCVV2] - "111"
 */
@Keep
@Parcelize
data class PaymentCard(
    val cardNum: String,
    val expYear: String,
    val expMonth: String,
    val cardCVV2: String
) : Parcelable
