package com.tranzzo.checkout.cardview.result

import androidx.annotation.Keep

@Keep
sealed class PaymentCardResult<out SUCCESS, out ERROR> {

    data class Success<out SUCCESS>(val success: SUCCESS) : PaymentCardResult<SUCCESS, Nothing>()

    data class Error<out ERROR>(val error: ERROR?) : PaymentCardResult<Nothing, ERROR>()

    fun result(onSuccess: (SUCCESS) -> Unit, onError: (ERROR?) -> Unit): Any =
        when (this) {
            is Success -> onSuccess(success)
            is Error -> onError(error)
        }
}