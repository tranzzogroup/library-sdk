package cards.pay.paycardsrecognizer.sdk.ui

import java.io.Serializable

data class ScanCardRequest(val mEnableSound:Boolean = false,
                           val mScanExpirationDate:Boolean = false,
                           val mScanCardHolder:Boolean = false,
                           val mGrabCardImage:Boolean = false
                                 ):Serializable {
companion object{
    fun getDefault() = ScanCardRequest()


    const val DEFAULT_ENABLE_SOUND = true

    const val DEFAULT_SCAN_EXPIRATION_DATE = true

    const val DEFAULT_SCAN_CARD_HOLDER = true

    const val DEFAULT_GRAB_CARD_IMAGE = false
}

    fun isScanCardHolderEnabled() = mScanCardHolder
    fun isScanExpirationDateEnabled() = mScanExpirationDate
    fun isGrabCardImageEnabled() = mGrabCardImage
    fun isSoundEnabled() = mEnableSound
}